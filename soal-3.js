function balancedBracket(s) {
  const stack = [];
  const openBrackets = ['(', '[', '{'];
  const closeBrakets = [')', ']', '}'];
  const pairBrackets = { ')': '(', ']': '[', '}': '{' };

  for (let i = 0; i < s.length; i++) {
    const currentBracket = s[i];

    if (openBrackets.includes(currentBracket)) {
      stack.push(currentBracket);
    } else if (closeBrakets.includes(currentBracket)) {
      const lastOpenedBracket = stack.pop();

      if (lastOpenedBracket !== pairBrackets[currentBracket]) {
        return 'NO';
      }
    }
  }

  return stack.length === 0 ? 'YES' : 'NO';
}

// Contoh penggunaan:
const case1 = '{[()]}';
const case2 = '{[(])}';
const case3 = '{(([])[ ])[ ]}';

console.log(balancedBracket(case1)); // Output: 'YES'
console.log(balancedBracket(case2)); // Output: 'NO'
console.log(balancedBracket(case3)); // Output: 'YES'
