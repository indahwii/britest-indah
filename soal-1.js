function calculateWeights(s) {
  const weights = [];
  let currentChar = s[0];
  let currentWeight = currentChar.charCodeAt(0) - 'a'.charCodeAt(0) + 1;
  let charCount = 0;

  for (let i = 0; i < s.length; i++) {
    if (s[i] === currentChar) {
      charCount++;
    } else {
      currentChar = s[i];
      currentWeight = currentChar.charCodeAt(0) - 'a'.charCodeAt(0) + 1;
      charCount = 1;
    }
    
    weights.push(currentWeight * charCount);
  }

  return weights;
}


function weightedStrings(s, queries) {
  const weights = calculateWeights(s);
  const results = [];

  for (const query of queries) {
    if (weights.includes(query)) {
      results.push('Yes');
    } else {
      results.push('No');
    }
  }

return results;
}

// Contoh penggunaan:
const inputString = 'abbcccd';
const inputQueries = [1, 3, 9, 8];

const output = weightedStrings(inputString, inputQueries);
console.log(output); // Output: ['Yes', 'Yes', 'Yes', 'No']