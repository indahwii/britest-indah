function palindromeHigh(str, k, start, end) {
  if (start >= end) {
    return str;
  }

  if (str[start] !== str[end]) {
    const higherChar = Math.max(str[start], str[end]);
    const newStr =
      str.substring(0, start) + higherChar + str.substring(start + 1, end) + higherChar + str.substring(end + 1);

    const palindromeResult = palindromeHigh(newStr, k - 1, start + 1, end - 1);

    if (palindromeResult !== -1) {
      return palindromeResult;
    }
  }

  return palindromeHigh(str, k, start + 1, end - 1);
}

function palindromeResult(string, k) {
  const result = palindromeHigh(string, k, 0, string.length - 1);
  return result !== -1 ? result : -1;
}

const inputString = '3943';
const k = 1;

const result = palindromeResult(inputString, k);
console.log(result); // Output: '3993'
